## Title

  Visual Nuts Print

## Technical Environment Rules

  General notes: For all exercises we expect actual runnable Java code and test to be provided.
  Imagine that all code you provide here is of the highest importance for the company, and target
  core features of our product. So the provided code should be up to production level quality.

## Goal

    Write or describe an algorithm that prints the whole integer numbers to the console, start
    from the number 1, and print all numbers going up to the number 100.
    However, when the number is divisible by 3, do not print the number, but print the word
    'Visual'. If the number is divisible by 5, do not print the number, but print 'Nuts'. And for all
    numbers divisible by both (eg: the number 15) the same, but print 'Visual Nuts'.
    How will you keep this code safe from bugs? Show how you would guarantee that this code
    keeps working when developers start making small feature adjustments. (Maybe we would
    want to print the first 500 numbers, ...).

    Writing tests!

## Tooling

  1. Please install IntelliJ or Eclipse Lombok plugin, to avoid possible compilation problems;
  2. Java 11;
  3. Maven;


# First Steps

  1. You can start the application running : mvn spring-boot:run
  1. The application will process the print process from print.start.number to the print.last.number. Both configured in application.properties file.
  2. The application.properties files can be found in Src -> Main -> Resources.
  3. The default start number is 1.
  3. The default last number is 100.

## Tech Stack

  1. Spring Framework

      - Spring Boot, Sprint Test
      - Please check POM for more information

  2. Lombok Plugin :

      - Reduce Boiler Plate code

  3. CheckStyle :

      - Code style pattern plugin (checkstyle.xml)

## Tests

  1. Unit and Integration Tests :

    - Folder : Src -> Test -> Java -> Visual.Nuts.Print -> unittests

  2. Integration Tests :

    - Folder : Src -> Test -> Java -> Visual.Nuts.Print -> integrationtests

  3. Tests Support Classes :

    - Folder : Src -> Test -> Java -> Visual.Nuts.Print -> testsupport


## GIT Lab

  If you want to login in to validate the project in GITLab you can use this login / password :

    1 - User : ngomesgitlab
    2 - Password : ngomesgitlab@1234

## Design Patterns

  1. I like to use Chain of Responsibility Design Pattern during validation process to de-couple one validation from another and to avoid create a LOT of if´s.