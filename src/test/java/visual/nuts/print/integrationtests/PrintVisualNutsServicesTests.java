package visual.nuts.print.integrationtests;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import visual.nuts.print.PrintVisualNutsApplication;
import visual.nuts.print.services.PrintVisualNutsServices;
import visual.nuts.print.testsupport.helpers.LogHelpMethods;
import visual.nuts.print.testsupport.validations.ValidatePrintLogs;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PrintVisualNutsApplication.class})
@ContextConfiguration
class PrintVisualNutsServicesTests {

    @Autowired
    private PrintVisualNutsServices printVisualNutsServices;

    @Test
    void shouldGenerateAndValidateLogProcessFrom1To1() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        printVisualNutsServices.printVisualNutsServices(1, 1);

        ValidatePrintLogs.validateLogsFrom1To1(loggingEventList);

    }

    @Test
    void shouldGenerateAndValidateLogProcessFrom1To3() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        printVisualNutsServices.printVisualNutsServices(1, 3);

        ValidatePrintLogs.validateLogsFrom1To3(loggingEventList);

    }

    @Test
    void shouldGenerateAndValidateLogProcessFrom1To5() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        printVisualNutsServices.printVisualNutsServices(1, 5);

        ValidatePrintLogs.validateLogsFrom1To5(loggingEventList);

    }

    /**
     * 15 is the first number that can divided by 3 and 5;
     */
    @Test
    void shouldGenerateAndValidateLogProcessFrom1To15() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        printVisualNutsServices.printVisualNutsServices(1, 15);

        ValidatePrintLogs.validateLogsFrom1To15(loggingEventList);

    }


}
