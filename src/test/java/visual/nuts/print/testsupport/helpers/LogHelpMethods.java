package visual.nuts.print.testsupport.helpers;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import org.junit.jupiter.api.Assertions;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class LogHelpMethods  {

    /**
     * Help Validate Log Classes
     */

    public static Function<Level, List<ILoggingEvent>> startGetLoggerMessagesWithLevel = (level) -> {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        if (nonNull(level)) {
            root.setLevel(level);
        }

        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        root.addAppender(listAppender);

        return listAppender.list;
    };


    public static BiFunction<List<ILoggingEvent>, Object, List<ILoggingEvent>> getLoggingEventsThatContainsString =
            (list, object) -> list
                .stream()
                .filter(x -> x.getMessage().contains(String.valueOf(object)))
                .collect(Collectors.toList());


    public static BiConsumer<List<ILoggingEvent>, Object> loggingEventsContainsString =
        (list, object) -> {
            Assertions.assertTrue(list.stream()
                .anyMatch(x -> x.getMessage().contains(String.valueOf(object))));
        };

}
