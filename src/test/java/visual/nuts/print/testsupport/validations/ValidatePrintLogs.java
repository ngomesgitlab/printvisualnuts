package visual.nuts.print.testsupport.validations;

import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.Assertions;
import visual.nuts.print.testsupport.helpers.LogHelpMethods;

import java.util.List;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.NUTS;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL_NUTS;

public class ValidatePrintLogs {

    public static void validateLogsFrom1To1(List<ILoggingEvent> loggingEventList) {

        /**
         * Print Numbers, no especial rule applied
         */
        validateIfNumberIsPresent(loggingEventList, 1, 1);

        /**
         * Stopped in 1
         */
        validateIfNumberIsNotPresent(loggingEventList, 2);

    }

    public static void validateLogsFrom1To3(List<ILoggingEvent> loggingEventList) {

        /**
         * Print Numbers, no especial rule applied
         */
        validateIfNumberIsPresent(loggingEventList, 1, 1);
        validateIfNumberIsPresent(loggingEventList, 2, 1);

        /**
         * 3 Should Contain VISUAL not the Number
         */
        validateIfNumberIsPresent(loggingEventList, VISUAL, 1);
        validateIfNumberIsNotPresent(loggingEventList, 3);

        /**
         * Stopped in 3
         */
        validateIfNumberIsNotPresent(loggingEventList, 4);

    }

    public static void validateLogsFrom1To5(List<ILoggingEvent> loggingEventList) {

        /**
         * Print Numbers, no especial rule applied
         */
        validateIfNumberIsPresent(loggingEventList, 1, 1);
        validateIfNumberIsPresent(loggingEventList, 2, 1);
        validateIfNumberIsPresent(loggingEventList, 4, 1);

        /**
         * 3 Should Contain VISUAL not the Number
         */
        validateIfNumberIsPresent(loggingEventList, VISUAL, 1);
        validateIfNumberIsNotPresent(loggingEventList, 3);

        /**
         * 5 Should Contain NUTS not the Number
         */
        validateIfNumberIsPresent(loggingEventList, NUTS, 1);
        validateIfNumberIsNotPresent(loggingEventList, 5);

        /**
         * Stopped in 5
         */
        validateIfNumberIsNotPresent(loggingEventList, 6);

    }

    public static void validateLogsFrom1To15(List<ILoggingEvent> loggingEventList) {

        /**
         * Print Numbers, no especial rule applied
         */
        validateIfNumberIsPresent(loggingEventList, 1, 4); //1, 11, 13, 14
        validateIfNumberIsPresent(loggingEventList, 2, 1);
        validateIfNumberIsPresent(loggingEventList, 4, 2); //4, 14
        validateIfNumberIsPresent(loggingEventList, 7, 1);
        validateIfNumberIsPresent(loggingEventList, 8, 1);
        validateIfNumberIsPresent(loggingEventList, 11, 1);
        validateIfNumberIsPresent(loggingEventList, 13, 1);
        validateIfNumberIsPresent(loggingEventList, 14, 1);

        /**
         * 3, 6, 9, 12 Should Contain VISUAL not the Number
         * 15 also contains NUTS
         */
        validateIfNumberIsPresent(loggingEventList, VISUAL, 5);
        validateIfNumberIsNotPresent(loggingEventList, 6);
        validateIfNumberIsNotPresent(loggingEventList, 9);
        validateIfNumberIsNotPresent(loggingEventList, 12);

        /**
         * 5, 10, 15 Should Contain VISUAL not the Number
         * 15 also contains VISUAL
         */
        validateIfNumberIsPresent(loggingEventList, NUTS, 3);
        validateIfNumberIsNotPresent(loggingEventList, 5);
        validateIfNumberIsNotPresent(loggingEventList, 10);

        /**
         * 15 Should contain VISUAL NUTS not the Number
         */
        validateIfNumberIsPresent(loggingEventList, VISUAL_NUTS, 1);
        validateIfNumberIsNotPresent(loggingEventList, 15);

        /**
         * Stopped in 15
         */
        validateIfNumberIsNotPresent(loggingEventList, 16);

    }





    private static void validateIfNumberIsPresent (
        List<ILoggingEvent> loggingEventList,
        Object objectExpected,
        Integer numberEntriesExpected) {

        List<ILoggingEvent> logListNumber = LogHelpMethods.getLoggingEventsThatContainsString.apply(loggingEventList, objectExpected);
        Assertions.assertNotNull(logListNumber);
        Assertions.assertFalse(logListNumber.isEmpty());
        Assertions.assertEquals(numberEntriesExpected, logListNumber.size());

    }

    private static void validateIfNumberIsNotPresent (
        List<ILoggingEvent> loggingEventList,
        Object objectExpected) {

        List<ILoggingEvent> logListNumber = LogHelpMethods.getLoggingEventsThatContainsString.apply(loggingEventList, objectExpected);
        Assertions.assertNotNull(logListNumber);
        Assertions.assertTrue(logListNumber.isEmpty());

    }



}
