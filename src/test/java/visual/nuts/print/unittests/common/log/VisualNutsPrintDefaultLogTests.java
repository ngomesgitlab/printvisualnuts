package visual.nuts.print.unittests.common.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import visual.nuts.print.PrintVisualNutsApplication;
import visual.nuts.print.common.log.VisualNutsPrintDefaultLog;
import visual.nuts.print.testsupport.helpers.LogHelpMethods;

import java.util.List;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.ENDING_VISUAL_NUTS_PRINTING_SERVICES;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_LAST_NUMBER;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_START_NUMBER;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.STARTING_VISUAL_NUTS_PRINTING_SERVICES;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PrintVisualNutsApplication.class})
@ContextConfiguration
class VisualNutsPrintDefaultLogTests {


    /**
     * logNumberOrMessage
     */

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateLogWithMessage() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        VisualNutsPrintDefaultLog.logNumberOrMessage(VISUAL);

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, VISUAL);


    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateLogWithNumber() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.INFO);

        VisualNutsPrintDefaultLog.logNumberOrMessage(PRINT_START_NUMBER);

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, PRINT_START_NUMBER);


    }


    /**
     * logNumberOrMessage
     */

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateStartProcessLog() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.DEBUG);

        VisualNutsPrintDefaultLog.logControlProcess(STARTING_VISUAL_NUTS_PRINTING_SERVICES, PRINT_START_NUMBER, PRINT_LAST_NUMBER);

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, STARTING_VISUAL_NUTS_PRINTING_SERVICES.substring(0, 15));
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, PRINT_START_NUMBER);
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, PRINT_LAST_NUMBER);

    }

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldGenerateEndProcessLog() {

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.DEBUG);

        VisualNutsPrintDefaultLog.logControlProcess(ENDING_VISUAL_NUTS_PRINTING_SERVICES, PRINT_START_NUMBER, PRINT_LAST_NUMBER);

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, ENDING_VISUAL_NUTS_PRINTING_SERVICES.substring(0, 15));
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, PRINT_START_NUMBER);
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, PRINT_LAST_NUMBER);

    }


}
