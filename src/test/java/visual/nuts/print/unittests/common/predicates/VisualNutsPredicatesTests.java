package visual.nuts.print.unittests.common.predicates;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import visual.nuts.print.PrintVisualNutsApplication;
import visual.nuts.print.common.predicates.VisualNutsPredicates;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PrintVisualNutsApplication.class})
@ContextConfiguration
class VisualNutsPredicatesTests {


    /**
     * quotientIsAIntegerIfDividedByThree
     */

    @Test
    void should_Be_False_If_Quotient_Is_Not_Zero_When_Divided_By_Three() {

        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(0));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(5));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(10));

    }

    @Test
    void should_Be_True_If_Quotient_Is_Zero_When_Divided_By_Three() {

        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(3));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(6));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(24));

    }

    /**
     * quotientIsAIntegerIfDividedByFive
     */

    @Test
    void should_Be_False_If_Quotient_Is_Not_Zero_When_Divided_By_Five() {

        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(0));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(3));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(6));

    }

    @Test
    void should_Be_True_If_Quotient_Is_Zero_When_Divided_By_Five() {

        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(5));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(15));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByFive.test(30));

    }


    /**
     * quotientIsAIntegerIfDividedByThreeAndFive
     */

    @Test
    void should_Be_False_If_Quotient_Is_Not_Zero_When_Divided_By_Three_And_Five() {

        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(0));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(3));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(5));
        Assertions.assertFalse(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(24));

    }

    @Test
    void should_Be_True_If_Quotient_Is_Zero_When_Divided_By_Three_And_Five() {

        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(15));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(30));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(60));
        Assertions.assertTrue(VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(120));

    }

}
