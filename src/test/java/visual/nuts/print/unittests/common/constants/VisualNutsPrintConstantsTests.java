package visual.nuts.print.unittests.common.constants;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import visual.nuts.print.PrintVisualNutsApplication;
import visual.nuts.print.common.predicates.GenericPredicates;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.NUTS;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_LAST_NUMBER;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_START_NUMBER;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL_NUTS;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PrintVisualNutsApplication.class})
@ContextConfiguration
class VisualNutsPrintConstantsTests {


    @Test
    void should_Be_Valid_Start_Number() {

        Assertions.assertNotNull(PRINT_START_NUMBER);

    }

    @Test
    void should_Be_Valid_End_Number() {

        Assertions.assertNotNull(PRINT_LAST_NUMBER);

    }

    @Test
    void should_Be_Valid_String() {

        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(VISUAL));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(NUTS));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(VISUAL_NUTS));

    }


}
