package visual.nuts.print.common.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class VisualNutsPrintConstants {

    @Value("${print.start.number}")
    public void setPrintStartNumber(Integer printStartNumber) {

        if (printStartNumber == null) {
            PRINT_START_NUMBER = 1;
        } else {
            PRINT_START_NUMBER = printStartNumber;
        }

    }

    @Value("${print.last.number}")
    public void setPrintLastNumber(Integer printLastNumber) {

        if (printLastNumber == null) {
            PRINT_LAST_NUMBER = 1;
        } else {
            PRINT_LAST_NUMBER = printLastNumber;
        }

    }


    /**
     * FROM ENV VARIABLES
     * You will be able to change the Start or Last Print Number in application properties file
     */

    public static Integer PRINT_START_NUMBER;
    public static Integer PRINT_LAST_NUMBER;

    /**
     * Constants
     */
    public static final String VISUAL_NUTS = "Visual Nuts";
    public static final String VISUAL = "Visual";
    public static final String NUTS = "Nuts";

    /**
     * Messages
     */
    public static final String STARTING_VISUAL_NUTS_PRINTING_SERVICES = "Starting executeVisualNutsServices from %s to %s";
    public static final String ENDING_VISUAL_NUTS_PRINTING_SERVICES = "Ending executeVisualNutsServices from %s to %s";


}
