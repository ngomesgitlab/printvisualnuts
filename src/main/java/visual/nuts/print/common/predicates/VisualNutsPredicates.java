package visual.nuts.print.common.predicates;

import java.util.function.IntPredicate;

public class VisualNutsPredicates {

    private VisualNutsPredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * In a Division, the divisor is the number of “people” that the number is being divided among
     */
    private static IntPredicate isValidDivisionDivisor = number -> number != 0;

    /**
     * In a Division, the quotient is the answer, in our case should Be A Integer
     */
    public static IntPredicate quotientIsAIntegerIfDividedByThree = number -> {

        if (isValidDivisionDivisor.negate().test(number)) return false;

        return number % 3 == 0;

    };

    /**
     * In a Division, the quotient is the answer, in our case should Be A Integer
     */
    public static IntPredicate quotientIsAIntegerIfDividedByFive = number -> {

        if (isValidDivisionDivisor.negate().test(number)) return false;

        return number % 5 == 0;

    };

    /**
     * In a Division, the quotient is the answer, in our case should Be A Integer
     */
    public static IntPredicate quotientIsAIntegerIfDividedByThreeAndFive = number -> quotientIsAIntegerIfDividedByThree.test(number) && quotientIsAIntegerIfDividedByFive.test(number);


}
