package visual.nuts.print.common.log;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VisualNutsPrintDefaultLog {

    private VisualNutsPrintDefaultLog() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static void logControlProcess(
        String message,
        int printStartNumber,
        int printLastNumber) {

        log.debug(String.format(
            message,
            printStartNumber,
            printLastNumber));

    }

    /**
     * Being Generic if we want to change we can change only in one place
     * Like if we want to start all outputs with "Logging : "
     */
    public static void logNumberOrMessage(Object logMessage) {

        log.info(String.valueOf(logMessage));

    }



}
