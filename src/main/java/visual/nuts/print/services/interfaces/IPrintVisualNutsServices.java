package visual.nuts.print.services.interfaces;

public interface IPrintVisualNutsServices {
    void printVisualNutsServices();

    void printVisualNutsServices(int printStartNumber, int printEndNumber);
}
