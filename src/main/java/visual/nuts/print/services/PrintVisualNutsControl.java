package visual.nuts.print.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import visual.nuts.print.common.log.VisualNutsPrintDefaultLog;
import visual.nuts.print.services.handles.DefaultPrintHandle;
import visual.nuts.print.services.handles.MultipleByFiveHandle;
import visual.nuts.print.services.handles.MultipleByThreeAndFiveHandle;
import visual.nuts.print.services.handles.MultipleByThreeHandle;
import visual.nuts.print.services.manager.IPrintVisualNutsValidationHandles;
import visual.nuts.print.services.manager.PrintVisualNutsValidationHandlesMap;

import javax.annotation.PostConstruct;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.ENDING_VISUAL_NUTS_PRINTING_SERVICES;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.STARTING_VISUAL_NUTS_PRINTING_SERVICES;

@Slf4j
@Service
@RequiredArgsConstructor
public class PrintVisualNutsControl {

    private final MultipleByFiveHandle multipleByFiveHandle;
    private final MultipleByThreeAndFiveHandle multipleByThreeAndFiveHandle;
    private final MultipleByThreeHandle multipleByThreeHandle;
    private final DefaultPrintHandle defaultPrintHandle;

    private PrintVisualNutsValidationHandlesMap printVisualNutsValidationHandlesMap;

    @PostConstruct
    private void startPrintVisualNutsHandles() {
        populatePrintVisualNutsHandles();
    }

    private void populatePrintVisualNutsHandles() {

        printVisualNutsValidationHandlesMap = new PrintVisualNutsValidationHandlesMap();
        printVisualNutsValidationHandlesMap.addHandle(multipleByThreeAndFiveHandle);
        printVisualNutsValidationHandlesMap.addHandle(multipleByFiveHandle);
        printVisualNutsValidationHandlesMap.addHandle(multipleByThreeHandle);
        printVisualNutsValidationHandlesMap.addHandle(defaultPrintHandle);

    }


    /**
     * Execute process , validate which handle can process the log
     */
    protected void executeVisualNutsServices(
        int printStartNumber,
        int printEndNumber) {

        VisualNutsPrintDefaultLog.logControlProcess(STARTING_VISUAL_NUTS_PRINTING_SERVICES, printStartNumber, printEndNumber);

        for (int number = printStartNumber; number <= printEndNumber; number++) {

            for (IPrintVisualNutsValidationHandles printVisualNutsValidationHandles : printVisualNutsValidationHandlesMap.handlesList()) {

                if (printVisualNutsValidationHandles.canHandle(number)) {

                    printVisualNutsValidationHandles.executeHandle(number);
                    break;

                }

            }

        }

        VisualNutsPrintDefaultLog.logControlProcess(ENDING_VISUAL_NUTS_PRINTING_SERVICES, printStartNumber, printEndNumber);

    }

}
