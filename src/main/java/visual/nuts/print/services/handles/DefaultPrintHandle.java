package visual.nuts.print.services.handles;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import visual.nuts.print.common.log.VisualNutsPrintDefaultLog;
import visual.nuts.print.services.manager.IPrintVisualNutsValidationHandles;

@Service
@Slf4j
public class DefaultPrintHandle implements IPrintVisualNutsValidationHandles {

    /**
     * Default Handle, will be executed last if other handles are not able to validate it
     */
    @Override
    public boolean canHandle(int currentNumber) {
        return true;
    }

    @Override
    public void executeHandle(int currentNumber) {
        VisualNutsPrintDefaultLog.logNumberOrMessage(currentNumber);
    }

}

