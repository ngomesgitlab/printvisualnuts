package visual.nuts.print.services.handles;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import visual.nuts.print.common.log.VisualNutsPrintDefaultLog;
import visual.nuts.print.common.predicates.VisualNutsPredicates;
import visual.nuts.print.services.manager.IPrintVisualNutsValidationHandles;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL_NUTS;

@Service
@Slf4j
public class MultipleByThreeAndFiveHandle implements IPrintVisualNutsValidationHandles {

    /**
     * Will be able to handle if number / 3 and 5 quotient == 0
     */
    @Override
    public boolean canHandle(int currentNumber) {
        return VisualNutsPredicates.quotientIsAIntegerIfDividedByThreeAndFive.test(currentNumber);
    }

    @Override
    public void executeHandle(int currentNumber) {
        VisualNutsPrintDefaultLog.logNumberOrMessage(VISUAL_NUTS);
    }

}
