package visual.nuts.print.services.handles;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import visual.nuts.print.common.log.VisualNutsPrintDefaultLog;
import visual.nuts.print.common.predicates.VisualNutsPredicates;
import visual.nuts.print.services.manager.IPrintVisualNutsValidationHandles;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.NUTS;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.VISUAL;

@Service
@Slf4j
public class MultipleByThreeHandle implements IPrintVisualNutsValidationHandles {

    /**
     * Will be able to handle if number / 5 quotient == 0
     */
    @Override
    public boolean canHandle(int currentNumber) {
        return VisualNutsPredicates.quotientIsAIntegerIfDividedByThree.test(currentNumber);
    }

    @Override
    public void executeHandle(int currentNumber) {

        VisualNutsPrintDefaultLog.logNumberOrMessage(VISUAL);
    }

}

