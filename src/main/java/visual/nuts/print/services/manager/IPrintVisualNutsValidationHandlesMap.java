package visual.nuts.print.services.manager;

import java.util.List;

public interface IPrintVisualNutsValidationHandlesMap<H> {

    List<H> handlesList();

    void addHandle(H handle);

}
