package visual.nuts.print.services.manager;

import java.util.ArrayList;
import java.util.List;

public class PrintVisualNutsValidationHandlesMap implements IPrintVisualNutsValidationHandlesMap<IPrintVisualNutsValidationHandles> {

    private final ArrayList<IPrintVisualNutsValidationHandles> handlesList;

    public PrintVisualNutsValidationHandlesMap() {
        handlesList = new ArrayList<>();
    }


    @Override
    public List<IPrintVisualNutsValidationHandles> handlesList() {
        return handlesList;
    }

    @Override
    public void addHandle(IPrintVisualNutsValidationHandles handle) {
        if (handle != null)
            handlesList.add(handle);
    }

}
