package visual.nuts.print.services.manager;


public interface IPrintVisualNutsValidationHandles {

    boolean canHandle(int currentNumber);

    void executeHandle(int currentNumber);

}
