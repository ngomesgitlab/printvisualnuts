package visual.nuts.print.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import visual.nuts.print.services.interfaces.IPrintVisualNutsServices;

import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_LAST_NUMBER;
import static visual.nuts.print.common.constants.VisualNutsPrintConstants.PRINT_START_NUMBER;

@Service
@RequiredArgsConstructor
public class PrintVisualNutsServices implements IPrintVisualNutsServices {

    private final PrintVisualNutsControl printVisualNutsControl;

    @Override
    public void printVisualNutsServices() {
        printVisualNutsControl.executeVisualNutsServices(PRINT_START_NUMBER, PRINT_LAST_NUMBER);
    }

    @Override
    public void printVisualNutsServices(int printStartNumber,
                                        int printEndNumber) {
        printVisualNutsControl.executeVisualNutsServices(printStartNumber, printEndNumber);
    }

}
