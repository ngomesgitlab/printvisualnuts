package visual.nuts.print;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import visual.nuts.print.services.PrintVisualNutsServices;

@SpringBootApplication
public class PrintVisualNutsApplication implements CommandLineRunner {

    @Autowired
    private PrintVisualNutsServices printVisualNutsServices;

    public static void main(String[] args) {
        SpringApplication.run(PrintVisualNutsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        printVisualNutsServices.printVisualNutsServices();
    }
}
